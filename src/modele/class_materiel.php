<?php
class Materiel{
    
    private $db;
    private $insert; // Étape 1
    private $select;
    
    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("insert  into  MATERIEL(nomMateriel, commentaire) values (:nomMateriel, :commentaire)"); // Étape 2                    
        $this->select = $db->prepare("select nomMateriel, commentaire from MATERIEL M");
    }

    public function insert($nomMateriel, $commentaire) { // Étape 3
        $r = true;
        $this->insert->execute(array(':nomMateriel'=>$nomMateriel, ':commentaire'=>$commentaire));
        if ($this->insert->errorCode()!=0){
            print_r($this->insert->errorInfo());  
            $r=false;
        }
        return $r;
    }
    
    
    public function select() {
        $listeM = $this->select->execute();
        if ($this->select->errorCode()!=0){
            print_r($this->select->errorInfo());  
        }
        return $this->select->fetchAll();
    }
}