<?php
class Activite{
    
    private $db;
    private $insert; // Étape 1
    private $select;
    
    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("insert  into  ACTIVITE(nomAct, descAct, idMateriel) values (:nomAct, :descAct, :idMateriel)"); // Étape 2                    
        $this->select = $db->prepare("select nomAct, descAct, M.nomMateriel from ACTIVITE A, MATERIEL M where A.idMateriel = M.codeMateriel order by nomMateriel");
    }

    public function insert($nomAct, $descAct, $idMateriel) { // Étape 3
        $r = true;
        $this->insert->execute(array(':nomAct'=>$nomAct, ':descAct'=>$descAct, ':idMateriel'=>$idMateriel));
        if ($this->insert->errorCode()!=0){
            print_r($this->insert->errorInfo());  
            $r=false;
        }
        return $r;
    }
    
    
    public function select() {
        $listeA = $this->select->execute();
        if ($this->select->errorCode()!=0){
            print_r($this->select->errorInfo());  
        }
        return $this->select->fetchAll();
    }
}