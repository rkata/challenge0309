<?php

class Client {

    private $db;
    private $insert; // Étape 1
    private $select;

    public function __construct($db) {//Demander pour id 
        $this->db = $db;
        $this->insert = $db->prepare("insert  into  CLIENT(email, mdp, nom, prenom, dateNaiss, cp, ville, rue, tel) values (:email, :mdp, :nom, :prenom, :dateNaiss, :cp, :ville, :rue, :tel)"); // Étape 2                    
        $this->select = $db->prepare("select idCli, nom, prenom, dateNaiss, cp, ville, rue, tel from CLIENT c");
    }

    public function insert($email, $mdp, $nom, $prenom, $dateNaiss, $cp, $ville, $rue, $tel) { // Étape 3
        $r = true;
        $this->insert->execute(array(':email' => $email, ':mdp' => $mdp, ':nom' => $nom, ':prenom' => $prenom, ':dateNaiss' => $dateNaiss, ':cp' => $cp, ':ville' => $ville, ':rue' => $rue, ':tel' => $tel));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function select() {
        $listeC = $this->select->execute();
        if ($this->select->errorCode() != 0) {
            print_r($this->select->errorInfo());
        }
        return $this->select->fetchAll();
    }

}
