<?php
class Presta{
    
    private $db;
    private $insert; // Étape 1
    private $select;
    
    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("insert into PRESTATION(typePrest, montant, idAct) values (:typePrest, :montant, :idAct)"); // Étape 2                    
        $this->select = $db->prepare("select typePrest, montant,idAct from PRESTATION P");
    }

    public function insert($typePrest, $montant,$idAct) { // Étape 3
        $r = true;
        $this->insert->execute(array(':typePrest'=>$typePrest, ':montant'=>$montant,':idAct'=>$idAct));
        if ($this->insert->errorCode()!=0){
            print_r($this->insert->errorInfo());  
            $r=false;
        }
        return $r;
    }
    
    
    public function select() {
        $listeP = $this->select->execute();
        if ($this->select->errorCode()!=0){
            print_r($this->select->errorInfo());  
        }
        return $this->select->fetchAll();
    }
}