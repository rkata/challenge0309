<?php

function getPage($db) {
    /* PAGES DE BASE */
    $lesPages['index'] = "actionIndex;0";
    $lesPages['maintenance'] = "actionMaintenance;0";
    $lesPages['activite'] = "actionActivite;0";
    $lesPages['prestation'] = "actionPresta;0";
    $lesPages['clients'] = "actionClient;0";
    $lesPages['priserdv'] = "actionRdv;0";
    $lesPages['ajouterPresta'] = "actionAjouterPresta;0";
    $lesPages['inscription'] = "actionInscription;0";


    if ($db != null) {
        if (isset($_GET['page'])) {
            $page = $_GET['page']; // Nous mettons dans la variable $page, la valeur qui a été passée dans le lien
        } else { // S'il n'y a rien en mémoire, nous lui donnons la valeur « accueil » afin de lui afficher une page par défaut
            $page = 'index';
        }
        if (!isset($lesPages[$page])) { // Nous rentrons ici si cela n'existe pas, ainsi nous redirigeons l'utilisateur sur la page d'accueil
            $page = 'index';
        }

        $explose = explode(";", $lesPages[$page]); // Nous découpons la ligne du tableau sur le caractère « ; » Le résultat est stocké dans le tableau $explose
        $role = $explose[1]; // Le rôle est dans la 2ème partie du tableau $explose

        if ($role != 0) { // Si mon rôle nécessite une vérification
            if (isset($_SESSION['login'])) { // Si je me suis authentifié
                if (isset($_SESSION['role'])) { // Si j’ai bien un rôle
                    if ($role != $_SESSION['role']) { // Si mon rôle ne correspond pas à celui qui est nécessaire pour voir la page
                        $contenu = 'actionIndex'; // Je le redirige vers l’accueil, car il n’a pas le bon rôle
                    } else {
                        $contenu = $explose[0]; // Je récupère le nom du contrôleur, car il a le bon rôle
                    }
                } else {
                    $contenu = 'actionIndex';
                }
            } else {
                $contenu = 'actionIndex'; // Page d’accueil, car il n’est pas authentifié
            }
        } else {
            $contenu = $explose[0]; // Je récupère le contrôleur, car il n’a pas besoin d’avoir un rôle
        }
    } else { // Si $db est null
        $contenu = 'actionMaintenance';
    }

    return $contenu;
}

?>
