<?php

function actionIndex($twig, $db) {
    $form = array();
    $presta = new Presta($db);
    $listeP = $presta->select();
    echo $twig->render('index.html.twig', array('form' => $form, 'listeP' => $listeP));
}

function actionDeconnexion() {
    session_unset();
    session_destroy();
    header("Location: ../web/");
}

function actionMaintenance($twig) {
    echo $twig->render('maintenance.html.twig', array());
}

function actionConnexion($twig, $db) {
    $form = array();

    if (isset($_POST['btConnexion'])) {
        $email = filter_input(INPUT_POST, 'emailConnexion', FILTER_SANITIZE_EMAIL);
        $mdp = filter_input(INPUT_POST, 'mdpConnexion', FILTER_SANITIZE_SPECIAL_CHARS);
        $client = new Client($db);
        $unClient = $client->connect($email, $mdp);

        if ($unClient != null) {
            if (!password_verify($mdp, $unClient['mdpConnexion'])) {
                $form['valide'] = false;
                $form['message'] = 'Mot de passe incorrect';
            } else {
                $form['valide'] = true;
                $_SESSION['email'] = $email;
                $_SESSION['mdp'] = $mdp;
            }
        } elseif ($unClient == false) {
            $form['valide'] = false;
            $form['message'] = 'Login incorrect';
        }
    }
    echo $twig->render('index.html.twig', array('form' => $form, 'session' => $_SESSION));
}


function actionPrendreRDV($twig, $db) {
    $form = array();
    if (isset($_POST['btPrendreRDV'])) {
        $typePresta = htmlspecialchars($_POST['typePresta']);
        $montant = htmlspecialchars($_POST['montant']);
        $idAct = htmlspecialchars($_POST['idAct']);
        $form['valide'] = true;

        $presta = new Presta($db);
        $exec = $presta->insert($typePresta, $montant, $idAct);
        if (!$exec) {
            $form['valide'] = false;
            $form['message'] = 'Problème d\'insertion dans la table Prestation ';
        }
    }
    echo $twig->render('priserdv.html.twig', array('form' => $form));
}
