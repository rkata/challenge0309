<?php

function actionPresta($twig, $db) {
    $form = array();
    $presta = new Presta($db);
    $listeP = $presta->select();
    echo $twig->render('prestation.html.twig', array('form' => $form, 'listeP' => $listeP));
}

function actionAjouterPresta($twig, $db) {
    $form = array();
    $presta = new Presta($db);
    $listeP = $presta->select();
    $activite = new Activite($db);
    $listeA = $activite->select();
    $typePrest = htmlspecialchars($_POST['typePrest']);
    $montant = htmlspecialchars($_POST['montant']);
    $idAct = htmlspecialchars($_POST['activite']);

    
    $exec = $presta->insert($typePrest, $montant, $idAct);
    var_dump($exec);
    var_dump($typePrest);
    var_dump($montant);
    var_dump($idAct);
    echo $twig->render('ajouterPresta.html.twig', array('form' => $form, 'listeP' => $listeP, 'listeA' => $listeA));
}
