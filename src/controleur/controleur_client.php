<?php

function actionClient($twig, $db) {
    $form = array(); 
    $clients = new Client($db);
    $listeC = $clients->select();
    echo $twig->render('clients.html.twig', array('form'=>$form,'listeC'=>$listeC));
}

function actionInscription($twig, $db) {
    $form = array();
    $r= true;

    if (isset($_POST['btInscription'])) {
        
        $email = htmlspecialchars($_POST['emailInscription']);
        $mdp1 = htmlspecialchars($_POST['mdp1']);
        $mdp2 = htmlspecialchars($_POST['mdp2']);
        $nom = htmlspecialchars($_POST['nom']);
        $prenom = htmlspecialchars($_POST['prenom']);
        $dateNaiss = htmlspecialchars($_POST['dateNaiss']);
        $cp = htmlspecialchars($_POST['cp']);
        $ville = htmlspecialchars($_POST['ville']);
        $rue = htmlspecialchars($_POST['rue']);
        $telephone = htmlspecialchars($_POST['telephone']);
        $form['valide'] = true;

        $client = new Client($db);
        
        if($mdp1 != $mdp2) {
            $form['valide'] = false;
            $form['message'] = 'Les mots de passe sont différents';
        } elseif (strlen($mdp1) < 8) {
            $form['valide'] = false;
            $form['message'] = 'Votre mot de passe est trop court il doit contenir au minimum 8 caractères';
        } else {
            $_SESSION['emailInscription'] = $email;
            $client = new Client($db);
            $exec = $client->insert($email, password_hash($mdp1, PASSWORD_DEFAULT), $nom, $prenom, $dateNaiss, $cp, $ville, $rue, $telephone);

            if (!$exec) {
                $form['valide'] = false;
                $form['message'] = 'Veuillez vérifier les informations saisies.';
            } else {
                $form['valide'] = true;
                $form['message'] = 'Vous pouvez maintenant vous connecter avec vos identifiants.';

//Envoie d'un mail de confirmation//
                $header = "MIME-Version: 1.0\r\n";
                $header .= 'From:"CabinetDuDocteur.com"<no-reply@CDD.com>' . "\n";
                $header .= 'Content-Type:text/html; charset="uft-8"' . "\n";
                $header .= 'Content-Transfer-Encoding: 8bit';

                $message = "
                           <html>
                                <body>
                                         <div align='center'>
                                                Bienvenue $prenom.<br/>
                                                <p> Le cabinet du docteur est content de vous compter dans ses clients.
                                                <a href='si6.arras-sio.com/web/index.php'> Veuillez vous rendre sur le site pour prendre rendez-vous.</a> </p>

                                        </div>
                                </body>
                          </html>
                          ";

                mail($email, "Cabinet du Docteur", $message, $header);
//fin d'envoie du mail//
                actionDeconnexion($twig);
            }
        }
        $form['emailInscription'] = $email;
    }
    var_dump($form['message']);
    var_dump($r);
    echo $twig->render('inscription.html.twig', array('form' => $form, 'session' => $_SESSION));
}
